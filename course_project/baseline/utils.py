from functools import reduce
import numpy as np

def get_random_state(seed):
    return np.random.RandomState(seed)


def random_boolean_1D_array(length, random_state):
    return random_state.choice([True, False], length)


def bit_flip(bit_string, random_state):
    neighbour = bit_string.copy()
    index = random_state.randint(0, len(neighbour))
    neighbour[index] = not neighbour[index]

    return neighbour


def parametrized_iterative_bit_flip(prob):
    def iterative_bit_flip(bit_string, random_state):
        neighbor = bit_string.copy()
        for index in range(len(neighbor)):
            if random_state.uniform() < prob:
                neighbor[index] = not neighbor[index]
        return neighbor

    return iterative_bit_flip


def random_float_1D_array(hypercube, random_state):
    return np.array([random_state.uniform(tuple_[0], tuple_[1])
                     for tuple_ in hypercube])


def random_float_cbound_1D_array(dimensions, l_cbound, u_cbound, random_state):
    return random_state.uniform(lower=l_cbound, upper=u_cbound, size=dimensions)


def parametrized_ball_mutation(radius):
    def ball_mutation(point, random_state):
        return np.array([random_state.uniform(low=coordinate - radius, high=coordinate + radius) for coordinate in point])
    return ball_mutation


def random_switch_member(percentage):
    def switch_member(point, random_state):
        for run in range(0,int(len(point)*percentage)):
            index1 = random_state.randint(low=0, high=len(point), size=1)
            index2 = random_state.randint(low=0, high=len(point), size=1)
            index2_value = point[index2]
            point[index2] = point[index1]
            point[index1] = index2_value
        return point
    return switch_member

def replace_with_random(percentage):
    def replace_random(point, random_state):
        for run in range(0, int(len(point) * percentage)):
            index = random_state.randint(low=0, high=len(point), size=1)
            point[index] = random_state.uniform(low=-2, high=2)
        return point
    return replace_random

def centre_inverse_mutation(point, random_state):
    return np.array(np.flip(point))

def random_inverse_mutation (point, random_state):
    index = int(random_state.randint(low=0, high=len(point), size=1))
    part_1 =  np.array(np.flip(point[:index]))
    part_2 = np.array(np.flip(point[index:]))
    return np.array([part_1, part_2])

def shuffle_mutation (point, random_state):
    return random_state.permutation(point)

def sphere_function(point):
    return np.sum(np.power(point, 2.), axis=len(point.shape) % 2 - 1)

def one_point_crossover(p1_r, p2_r, random_state):
    len_ = len(p1_r)
    point = random_state.randint(len_)
    off1_r = np.concatenate((p1_r[0:point], p2_r[point:len_]))
    off2_r = np.concatenate((p2_r[0:point], p1_r[point:len_]))
    return off1_r, off2_r

def two_point_crossover(p1_r, p2_r, random_state):
    len_ = len(p1_r)
    point1 = random_state.randint(len_)
    point2 = random_state.randint(len_)
    off1_r = np.concatenate((p1_r[0:point1], p2_r[point1:point2], p1_r[point2:len_]))
    off2_r = np.concatenate((p2_r[0:point1], p1_r[point1:point2], p2_r[point2:len_]))
    return off1_r, off2_r

def two_sided_geometric_crossover(p1_r, p2_r, random_state):
    off1_r, off2_r = np.zeros((len(p1_r),), dtype=int), np.zeros((len(p1_r),), dtype=int)
    for i in range(int(len(p1_r))):
        R = random_state.uniform()
        off1_r[i] = R*p1_r[i] + (1-R)*p2_r[i]
        off2_r[i] = R*p2_r[i] + (1-R)*p1_r[i]
    return off1_r, off2_r

def one_sided_geometric_crossover(p1_r, p2_r, random_state):
    off_r = np.zeros((len(p1_r),), dtype=float)
    for i in range(int(len(p1_r))):
        R = random_state.uniform()
        off_r[i] = R * p1_r[i] + (1 - R) * p2_r[i]
    return off_r

def parameterized_probability_crossover(probability):
    def probability_crossover (p1_r, p2_r, random_state):
        off1_r = np.zeros((len(p1_r),), dtype=float)
        off2_r = np.zeros((len(p1_r),), dtype=float)
        for element in range(0, len(p1_r)):
            if random_state.uniform() < probability:
                off1_r[element], off2_r[element] = p2_r[element], p1_r[element]
            else:
                off1_r[element], off2_r[element] = p1_r[element], p2_r[element]
        return off1_r, off2_r
    return probability_crossover

def generate_cbound_hypervolume(dimensions, l_cbound, u_cbound):
  return [(l_cbound, u_cbound) for _ in range(dimensions)]


def parametrized_ann(ann_i):
  def ann_ff(weights):
    return ann_i.stimulate(weights)
  return ann_ff


def parametrized_tournament_selection(pressure):
    def tournament_selection(population, minimization, random_state):
        tournament_pool_size = int(len(population)*pressure)
        tournament_pool = random_state.choice(population, size=tournament_pool_size, replace=False)

        if minimization:
            return reduce(lambda x, y: x if x.fitness <= y.fitness else y, tournament_pool)
        else:
            return reduce(lambda x, y: x if x.fitness >= y.fitness else y, tournament_pool)

    return tournament_selection


def roulette_wheel(population, minimization, random_state):

    n_iterations = int(len(population))
    fitness_values = []

    for individual in range(n_iterations):
        fitness_values.append(population[individual].fitness)

    if minimization:
        fitness_values = [1-p for p in fitness_values]

    fitness_sum = sum(fitness_values[0:len(fitness_values)])
    for individual in range(n_iterations):
        fitness_values[individual] = fitness_values[individual] / fitness_sum

    roulette_winner = random_state.choice(population, size=1, replace=False, p=fitness_values)

    return roulette_winner[0]




def ranking_selection(population, minimization, random_state):
    n_iterations = int(len(population))
    fitness_values = []

    for individual in range(n_iterations):
        fitness_values.append(population[individual].fitness)

    rank = [0] * len(fitness_values)
    if minimization:
        for i in range(1, len(fitness_values) + 1):
            rank[fitness_values.index(max(fitness_values))] = i
            fitness_values[fitness_values.index(max(fitness_values))] = min(fitness_values) - 1

    if not minimization:
        for i in range(1, len(fitness_values) + 1):
            rank[fitness_values.index(min(fitness_values))] = i
            fitness_values[fitness_values.index(min(fitness_values))] = max(fitness_values) + 1

    rank_sum = sum(rank[0:len(rank)])
    for individual in range(n_iterations):
        rank[individual] = rank[individual] / rank_sum

    rank_winner = random_state.choice(population, size=1, replace=False, p=rank)
    return rank_winner[0]


def best_rank_selection (population, minimization, random_state):
    return sorted(population, key = lambda x: x.fitness, reverse=True)[0]


def parametrized_boltzmann_selection(n_gen, T0, alpha):
    def boltzmann_selection_n(population, minimization, random_state, elite, iteration):
        accepted = 0
        while accepted == 0:
            X = random_state.choice(population, size=1, replace=False)[0]
            if (minimization and X.fitness <= elite.fitness) or (minimization == False and X.fitness >= elite.fitness):
                return X
            else:
                f_X, f_max = X.fitness, elite.fitness
                k = (1 + ((100*iteration)/n_gen))
                T = T0*((1-alpha)**k)
                P = np.exp(((f_max - f_X)/T))
                if minimization:
                    if random_state.uniform() > P:
                        return X
                else:
                    if random_state.uniform() < P:
                        return X
    return boltzmann_selection_n

def boltzmann_selection(pressure, n_gen):
    gen = 0
    def tournament_selection(population, minimization, random_state):
        nonlocal  gen
        factor = pressure - ((pressure / 2) * (gen/(n_gen*len(population))))
        gen = gen + 1
        tournament_pool_size = int(len(population) * factor)
        tournament_pool = random_state.choice(population, size=tournament_pool_size, replace=False)

        if minimization:
            return reduce(lambda x, y: x if x.fitness <= y.fitness else y, tournament_pool)
        else:
            return reduce(lambda x, y: x if x.fitness >= y.fitness else y, tournament_pool)

    return tournament_selection