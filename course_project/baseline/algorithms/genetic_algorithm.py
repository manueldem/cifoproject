import logging
import numpy as np
from functools import reduce
from scipy.spatial import distance

from random_search import RandomSearch
from solutions.solution import Solution


class GeneticAlgorithm(RandomSearch):
    def __init__(self, problem_instance, random_state, population_size,
                 selection, crossover, p_c, mutation, p_m, number_elites=0, random_member=False, gaussian_initialization=False):
        RandomSearch.__init__(self, problem_instance, random_state)
        self.population_size = population_size
        self.selection = selection
        self.crossover = crossover
        self.p_c = p_c
        self.mutation = mutation
        self.p_m = p_m
        self.number_elites = number_elites
        self.random_member = random_member
        self.gaussian_initialization = gaussian_initialization

    def initialize(self):
        self.population = self._generate_random_valid_solutions()
        self.best_solution = self._get_elite(self.population)


    def search(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution

        for iteration in range(n_iterations):
            offsprings = []

            while len(offsprings) < len(self.population):
                off1, off2 = p1, p2 = [
                    self.selection(self.population, self.problem_instance.minimization, self._random_state) for _ in range(2)]

                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings.extend([off1, off2])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness, elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings)]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite

    def search_with_euclidean(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution

        for iteration in range(n_iterations):
            offsprings = []

            while len(offsprings) < len(self.population):
                off1, off2 = p1, p2 = [
                    self.selection(self.population, self.problem_instance.minimization, self._random_state) for _ in
                    range(2)]

                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings.extend([off1, off2])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            #euclidean
            distance = self._euclidean_distance(offsprings)
            distance_pheno = self._euclidean_pheno(offsprings)

            if report:
                self._verbose_reporter_inner(elite, iteration, distance, distance_pheno)

            if log:
                log_event = [iteration, elite.fitness,
                             elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings), distance, distance_pheno]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite





    def search_with_one_crossover(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution

        for iteration in range(n_iterations):
            offsprings = []
            offsprings.extend(self._get_number_elites(self.population, self.number_elites))

            while len(offsprings) < len(self.population):
                p1, p2 = [
                    self.selection(self.population, self.problem_instance.minimization, self._random_state) for _ in
                    range(2)]

                if self._random_state.uniform() < self.p_c:
                    off = self._crossover_one(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off = self._mutation(off)

                if not (hasattr(off, 'fitness')):
                    self.problem_instance.evaluate(off)
                offsprings.extend([off])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            if self.random_member:
                random_member = self._generate_random_valid_solution()
                offsprings[int(self._random_state.randint(low=0, high=len(self.population), size=1))] = random_member

            offsprings.extend(self._get_number_elites(self.population, self.number_elites))
            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness,
                             elite.validation_fitness if hasattr(off, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings)]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite

    def search_for_Boltzmann(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution

        for iteration in range(n_iterations):
            offsprings = []

            while len(offsprings) < len(self.population):
                off1, off2 = p1, p2 = [
                    self.selection(self.population, self.problem_instance.minimization, self._random_state, elite, iteration) for _ in
                    range(2)]

                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings.extend([off1, off2])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness,
                             elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings),
                             self.random_member, self.number_elites]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite


    def search_with_elitism(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution

        for iteration in range(n_iterations):
            offsprings = []
            offsprings.extend(self._get_number_elites(self.population, self.number_elites))

            while len(offsprings) < len(self.population):
                off1, off2 = p1, p2 = [
                    self.selection(self.population, self.problem_instance.minimization, self._random_state) for _ in range(2)]

                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings.extend([off1, off2])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            if self.random_member:
                random_member = self._generate_random_valid_solution()
                offsprings[int(self._random_state.randint(low=0, high=len(self.population), size=1))] = random_member


            offsprings.extend(self._get_number_elites(self.population, self.number_elites))
            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness, elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings),
                             self.number_elites, self.random_member]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite

    def search_with_subpopulations(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution
        cutting_point = int((len(self.population) / 2))
        subpopulation_1 = np.array(self.population[:cutting_point])
        subpopulation_2 = np.array(self.population[cutting_point:])

        for iteration in range(n_iterations):
            offsprings_1 = []
            offsprings_2 = []


            while (len(offsprings_1) + len(offsprings_2)) < (len(subpopulation_1)+len(subpopulation_2)):
                #subpopulation 1
                off1, off2 = p1, p2 = [
                    self.selection(subpopulation_1, self.problem_instance.minimization, self._random_state) for _ in range(2)]

                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)
                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings_1.extend([off1, off2])

                #subpopulation 2
                off1, off2 = p1, p2 = [
                    self.selection(subpopulation_2, self.problem_instance.minimization, self._random_state) for _ in range(2)]
                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                   self.problem_instance.evaluate(off1)
                   self.problem_instance.evaluate(off2)
                offsprings_2.extend([off1, off2])

            while len(offsprings_1) > len(subpopulation_1):
                offsprings_1.pop()
            while len(offsprings_2) > len(subpopulation_2):
                offsprings_2.pop()

            elite_offspring_1, elite_offspring_2 = self._get_elite(offsprings_1), self._get_elite(offsprings_2)
            elite_all = self._get_best(elite_offspring_1, elite_offspring_2)
            elite = self._get_best(elite, elite_all)

            #elite swap
            offsprings_1[offsprings_1.index(elite_offspring_1)] = elite_offspring_2
            offsprings_2[offsprings_2.index(elite_offspring_2)] = elite_offspring_1


            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness,
                             elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings_1),
                             self._phenotypic_diversity_shift(offsprings_1)]
                logger.info(','.join(list(map(str, log_event))))

            subpopulation_1, subpopulation_2 = offsprings_1, offsprings_2

        self.best_solution = elite

    def search_parents(self, n_iterations, report=False, log=False):
        if log:
            log_event = [self.problem_instance.__class__, id(self._random_state), __name__]
            logger = logging.getLogger(','.join(list(map(str, log_event))))

        elite = self.best_solution
        distances = []
        for iteration in range(n_iterations):
            offsprings = []

            while len(offsprings) < len(self.population):
                p1 = self.selection(self.population, self.problem_instance.minimization, self._random_state)
                dst = 0
                while dst == 0:
                    p2 = self.selection(self.population, self.problem_instance.minimization, self._random_state)
                    dst = distance.euclidean(p1.representation, p2.representation)
                distances.append(dst)
                off1 = p1
                off2 = p2


                if self._random_state.uniform() < self.p_c:
                    off1, off2 = self._crossover(p1, p2)

                if self._random_state.uniform() < self.p_m:
                    off1 = self._mutation(off1)
                    off2 = self._mutation(off2)

                if not (hasattr(off1, 'fitness') and hasattr(off2, 'fitness')):
                    self.problem_instance.evaluate(off1)
                    self.problem_instance.evaluate(off2)
                offsprings.extend([off1, off2])

            while len(offsprings) > len(self.population):
                offsprings.pop()

            elite_offspring = self._get_elite(offsprings)
            elite = self._get_best(elite, elite_offspring)

            if report:
                self._verbose_reporter_inner(elite, iteration)

            if log:
                log_event = [iteration, elite.fitness, elite.validation_fitness if hasattr(off2, 'validation_fitness') else None,
                             self.population_size, self.selection.__name__, self.crossover.__name__, self.p_c,
                             self.mutation.__name__, None, None, self.p_m, self._phenotypic_diversity_shift(offsprings)]
                logger.info(','.join(list(map(str, log_event))))

            self.population = offsprings

        self.best_solution = elite

    def _crossover(self, p1, p2):
        off1, off2 = self.crossover(p1.representation, p2.representation, self._random_state)
        off1, off2 = Solution(off1), Solution(off2)
        return off1, off2

    def _crossover_one(self, p1, p2):
        off = self.crossover(p1.representation, p2.representation, self._random_state)
        off = Solution(off)
        return off

    def _mutation(self, individual):
        mutant = self.mutation(individual.representation, self._random_state)
        mutant = Solution(mutant)
        return mutant

    def _get_elite(self, population):
        elite = reduce(self._get_best, population)
        return elite

    def _phenotypic_diversity_shift(self, offsprings):
        fitness_parents = np.array([parent.fitness for parent in self.population])
        fitness_offsprings = np.array([offspring.fitness for offspring in offsprings])
        return np.std(fitness_offsprings)-np.std(fitness_parents)

    def _generate_random_valid_solutions(self):
        solutions = np.array([self._generate_random_valid_solution()
                              for i in range(self.population_size)])
        return solutions

    def _get_number_elites(self, population, individual):
        return sorted(population, key=lambda individual: individual.fitness, reverse=not self.problem_instance.minimization)[:individual]

    def _get_random_valid_solution(self):
        solution =  np.array([self._generate_random_valid_solution()])
        return solution

    def _euclidean_distance(self, offsprings):
        euclidean_distance = 0
        for offspring in offsprings:
            for point in offsprings:
                euclidean_distance = euclidean_distance + distance.euclidean(offspring.representation, point.representation)
        return euclidean_distance

    def _euclidean_pheno(self, offsprings):
        euclidean_distance = 0
        for offspring in offsprings:
            for point in offsprings:
                euclidean_distance = euclidean_distance + distance.euclidean(offspring.fitness,
                                                                             point.fitness)
        return euclidean_distance
