###### **Algorithms and Methods:**

### Genetic Algorithms:
- Selection Methods:
    - Parametrized Tournament Selection (Ilya)
    - Ranking Selection [X]
    - Roulette Wheel [X]
    
- Crossover Methods:
    - One Point Crossover (Ilya)
    - Two Point Crossover [X]

- Mutation Methods:
    - Parametrized Ball Mutation (Ilya)
    - Random Switch Member [X]
    - Centre Inverse Mutation [X]
    - Shuffle Mutation [X]
    
- Search Methods:
    - Classic Search Method (Ilya)
    - Search Parents (Potentially Cheating) [X]
    - Two Population with Elite Exchange [ ] -in progress